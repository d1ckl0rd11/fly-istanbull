package edu.aubg.istanbul;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by Dave on 05/03/2018.
 */


public class Imagefrag extends Fragment {
    private int picture;
    public View view;
    private ImageView img;
    LinearLayout lin;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        //Inflate the layout for this fragment
        picture = this.getArguments().getInt("pic");
        view = inflater.inflate(
                R.layout.imagefrag, container, false);
            img = (ImageView) view.findViewById(R.id.bigimg);
            lin = (LinearLayout) view.findViewById(R.id.poper);
            img.setImageResource(picture);
            lin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().beginTransaction().remove(Imagefrag.this).commit();
                }
            });
        return view;
    }
}
