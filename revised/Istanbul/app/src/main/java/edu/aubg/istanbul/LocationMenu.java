package edu.aubg.istanbul;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class LocationMenu extends AppCompatActivity {
    private RecyclerView rec;
    private RecyclerView.Adapter adapter;
    private List<Item> items;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_menu);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        rec = (RecyclerView) findViewById(R.id.holder);
        rec.setLayoutManager(new LinearLayoutManager(this));
        items = new ArrayList<>();
        //
        String title=getResources().getString(R.string.place0);
        String text = getResources().getString(R.string.description0);
        int video = R.raw.basilica0cistern;
        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.mipmap.basilica0cistern);
        images.add(R.mipmap.basilica0cistern1);
        images.add(R.mipmap.basilica0cistern2);
        Item item = new Item(title, images.get(0),text,video,images);
        items.add(item);//
        ///////////////////
        title=getResources().getString(R.string.place1);
        text=getResources().getString(R.string.description1);
        video=R.raw.beylerbeyi0palace;
        images=new ArrayList<>();
        images.add(R.mipmap.beylerbeyi0palace);
        images.add(R.mipmap.beylerbeyi0palace1);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////


        title=getResources().getString(R.string.place2);
        text=getResources().getString(R.string.description2);
        video=R.raw.bosphorus0bridge;
        images=new ArrayList<>();
        images.add(R.mipmap.bosphorus0bridge);
        images.add(R.mipmap.bosphorus0bridge1);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place3);
        text=getResources().getString(R.string.description3);
        video=R.raw.chora0church;
        images=new ArrayList<>();
        images.add(R.mipmap.chora0church);
        images.add(R.mipmap.chora0church1);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place4);
        text=getResources().getString(R.string.description4);
        video=R.raw.ciragan0palace;
        images=new ArrayList<>();
        images.add(R.mipmap.ciragan0palace);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place5);
        text=getResources().getString(R.string.description5);
        //video=R.raw.dolmabahce0palace;
        images=new ArrayList<>();
        images.add(R.mipmap.dolmabahce0palace);
        images.add(R.mipmap.dolmabahce0palace1);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place6);
        text=getResources().getString(R.string.description6);
        video=R.raw.eyup0sultan0mosque;
        images=new ArrayList<>();
        images.add(R.mipmap.eyup0sultan0mosque);
        images.add(R.mipmap.eyup0sultan0mosque1);
        images.add(R.mipmap.eyup0sultan0mosque2);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place7);
        text=getResources().getString(R.string.description7);
        //video=R.raw.galata0tow;
        images=new ArrayList<>();
        images.add(R.mipmap.galata0tower);
        images.add(R.mipmap.galata0tower1);
        images.add(R.mipmap.galata0tower2);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place8);
        text=getResources().getString(R.string.description8);
        video=R.raw.grand0bazaar;
        images=new ArrayList<>();
        images.add(R.mipmap.grand0bazaar);
        images.add(R.mipmap.grand0bazaar1);
        images.add(R.mipmap.grand0bazaar2);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place9);
        text=getResources().getString(R.string.description9);
        video=R.raw.hagia0irene;
        images=new ArrayList<>();
        images.add(R.mipmap.hagia0irene);
        images.add(R.mipmap.hagia0irene1);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place10);
        text=getResources().getString(R.string.description10);
        //video=R.raw.hagia0sophia;
        images=new ArrayList<>();
        images.add(R.mipmap.hagia0sophia);
        images.add(R.mipmap.hagia0sophia1);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);
        ///////////////////
        title=getResources().getString(R.string.place11);
        text=getResources().getString(R.string.description11);
        //video=R.raw.isthip;
        images=new ArrayList<>();
        images.add(R.mipmap.hippodrome0of0constantinople);
        images.add(R.mipmap.hippodrome0of0constantinople1);
        images.add(R.mipmap.hippodrome0of0constantinople2);
        images.add(R.mipmap.hippodrome0of0constantinople3);
        item=new Item(title,images.get(0),text,video,images);
        items.add(item);


        /////
        /*item = new Item("test7",R.mipmap.derp,"this is an experement",R.raw.thingie,test);
        String name = "Basilica Cistern";
        int image = R.drawable.maxresdefault;
        String desc = "The Basilica Cistern (Turkish: Yerebatan Sarnıcı – \"Cistern Sinking Into Ground\"), is the largest of several hundred ancient cisterns that lie beneath the city of Istanbul (formerly Constantinople), Turkey. The cistern, located 150 metres (490 ft) southwest of the Hagia Sophia on the historical peninsula of Sarayburnu, was built in the 6th century during the reign of Byzantine Emperor Justinian I.The Basilica Cistern (Turkish: Yerebatan Sarnıcı – \"Cistern Sinking Into Ground\"), is the largest of several hundred ancient cisterns that lie beneath the city of Istanbul (formerly Constantinople), Turkey. The cistern, located 150 metres (490 ft) southwest of the Hagia Sophia on the historical peninsula of Sarayburnu, was built in the 6th century during the reign of Byzantine Emperor Justinian I.";
        int videos = R.raw.thingie;
        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.maxresdefault);
        Item item2 = new Item(name,image,desc,videos,images);
        items.add(item2);*/
        adapter = new ItemAdapter(items, this);
        rec.setAdapter(adapter);
    }
}
