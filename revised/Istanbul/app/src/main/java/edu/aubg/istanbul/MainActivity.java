package edu.aubg.istanbul;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void SwitchToLocation(View view)
    {
        Intent intent;
        switch (view.getId()){
            case R.id.cardo:
                intent = new Intent(this,LocationMenu.class);break;
            case R.id.cardo1:
                intent =new Intent(this,food.class);break;
            case R.id.cardo2:
                intent =new Intent(this,phras.class);break;
            case R.id.cardo3:
                intent =
                        new Intent("android.intent.action.VIEW", Uri.parse("https://www.hotels.com/de1341107/hotels-istanbul-turkey/?destinationid=1341107&dateless=true&pos=HCOM_EMEA&locale=en_IE&rffrid=sem.hcom.EM.google.003.03.02.s.kwrd=c.171565803277.34588545306.679916587.1t1.kwd-2397732227.1001445..book%20hotels%20istanbul.EAIaIQobChMIwbLSosvb2QIV6L3tCh1KcQkWEAAYASAAEgJ9fvD_BwE.aw.ds&PSRC=AFF05&gclid=EAIaIQobChMIwbLSosvb2QIV6L3tCh1KcQkWEAAYASAAEgJ9fvD_BwE&gclsrc=aw.ds"));break;
            case R.id.cardo4:
                intent =
                        new Intent("android.intent.action.VIEW",Uri.parse("https://www.google.com/maps/place/Istanbul,+Turkey/@41.0052367,28.8720972,11z/data=!3m1!4b1!4m5!3m4!1s0x14caa7040068086b:0xe1ccfe98bc01b0d0!8m2!3d41.0082376!4d28.9783589?hl=en"));break;
                        default:
                Toast.makeText(this,"Unexpected error",Toast.LENGTH_LONG);
                intent = new Intent(this,LocationMenu.class);break;
        }
        startActivity(intent);
    }
}
