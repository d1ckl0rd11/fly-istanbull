package edu.aubg.istanbul;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.LauncherActivity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Dave on 04/03/2018.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    private List<Item> items;
    private Context context;

    public ItemAdapter(List<Item> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.card,parent,false);
    return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemAdapter.ViewHolder holder,int position) {
        final Item item = items.get(position);
        holder.name.setText(item.getName());
        holder.image.setImageResource(item.getImage());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {

                Fragment frag = new CityFragment();
                Bundle data = new Bundle();
                data.putInt("Video",item.getVideo());
                data.putString("Text",item.getText());
                data.putIntegerArrayList("Pictures",item.getPiclist());
                frag.setArguments(data);
                FragmentManager fragger = ((Activity)context).getFragmentManager();
                FragmentTransaction fragtrans = fragger.beginTransaction();
                fragtrans.add(R.id.fragter,frag).addToBackStack(null);
                fragtrans.commit();

            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public LinearLayout layout;
        public TextView fragtext;
        public TextView name;
        public ImageView image;
        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.img);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            fragtext = (TextView) itemView.findViewById(R.id.fragtext);
        }
    }
}
