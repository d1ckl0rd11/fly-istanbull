package edu.aubg.istanbul;

import android.content.Context;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dave on 05/03/2018.
 */

public class ImageAdapter extends BaseAdapter {
    private ArrayList<Integer> imagelist;
    private Context context;


    @Override
    public int getCount() {
        return imagelist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    public ImageAdapter(Context c,ArrayList<Integer> list) {
        context = c;
        imagelist = list;
    }


    public View getView(int i, View view, ViewGroup parent) {
        ImageView imageView;
        if (view == null) {

            // if it's not recycled, initialize some attributes
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(300, 300));

            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        }
     else {
        imageView = (ImageView) view;
        }

        imageView.setImageResource(imagelist.get(i));
        return imageView;

    }
}
